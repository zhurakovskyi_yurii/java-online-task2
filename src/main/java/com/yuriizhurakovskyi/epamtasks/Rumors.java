package com.yuriizhurakovskyi.epamtasks;

import java.util.ArrayList;

public class Rumors {
    private ArrayList<Boolean> allPeople;
    private int N;
    private int spread;
    private double probAllSpread;

    public Rumors(int N) {
        this.N = N;
        allPeople = new ArrayList<Boolean>();
        allPeople.add(true);
        for (int i = 0; i < N-1; i++) {
            allPeople.add(false);
        }
        spread = 1;
        probAllSpread = 1.0;
    }

    public void spreadRumors(){
        if(N!=0) {
            int sender = 0, recipient = (int)(Math.random() * (N - 1));
            while (true) {
                while (sender == recipient){
                    recipient = (int)(Math.random() * (N - 1));
                }
                if(allPeople.get(recipient) == false) {
                    allPeople.set(recipient, true);
                    spread++;
                } else break;

                sender = recipient;
                recipient = (int)(Math.random() * (N - 1));
            }
        }
    }
    public void showSpreaded(){
        if(spread!=1)
            System.out.println(spread + " people knows");
        else System.out.println("didn't spread yet");
    }

    public void getProbAllSpread(){
        for (int i = 1; i < N; i++)
        {
            probAllSpread = probAllSpread*(N-i)/(N-1);
        }
        System.out.println("The probability that everyone at the party" +
                " (except Alice) will hear the rumor before it stops propagating is: " + (double)Math.round(probAllSpread*10000000000000000L)/10000000000000000L
                        + " or " + (double)Math.round(probAllSpread*10000000000000000L)/10000000000000000L*100 + "%");
    }
}
