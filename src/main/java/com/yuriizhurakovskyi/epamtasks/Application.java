package com.yuriizhurakovskyi.epamtasks;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number of people:");
        Rumors r = new Rumors(scanner.nextInt());
        scanner.close();
        r.spreadRumors();
        r.getProbAllSpread();
        r.showSpreaded();
    }
}
